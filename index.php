<?php
// Bootstrap the application.
require_once(__DIR__ . '/vendor/autoload.php');

// Load WordPress.
require_once(__DIR__ . '/../../wp-load.php');

// Start the application.
$app = new \Slim\App([
    // Enable error details in the API when WP_DEBUG is true.
    'displayErrorDetails' => defined('WP_DEBUG') && WP_DEBUG
]);

// Middleware for parsing the Bearer token.
$app->add(new \Vreddo\Api\Rest\Middleware\TokenSession());

// Import routes.
require('src/routes.php');

$app->run();
