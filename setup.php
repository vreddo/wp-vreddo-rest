<?php
require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/../../wp-load.php');

$token_mapper = new \Vreddo\Api\Rest\Models\TokenMapper();
$token_mapper->bootstrap();