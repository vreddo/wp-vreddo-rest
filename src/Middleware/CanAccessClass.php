<?php
namespace Vreddo\Api\Rest\Middleware;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Vreddo\Api\Rest\Models\ClassesMapper;

/**
 * Middleware for ensuring a user can access the class in the request.
 */
class CanAccessClass
{
    protected $callback;

    public function __construct($callback = null) {
        if ($callback) {
            $this->callback = $callback;
        } else {
            $this->callback = function($request, $response) {
                // Retrieve the route parameters.
                $routeParams = $request->getAttribute('routeInfo')[2];

                return isset($routeParams['class']) ? $routeParams['class'] : null;
            };
        }
    }

    /**
     * Checks whether the current user can access the class in the request.
     *
     * @param Request $request
     * @param Response $response
     * @param callback $next
     * 
     * @return void
     */
    public function __invoke(Request $request, Response $response, $next) {
        $class_id = ($this->callback)($request, $response);

        // Check for the "class" parameter.
        if ($class_id) {
            $user = $request->getAttribute('user');

            // Check if they're a tutor or administrator.
            if (user_can($user, 'administrator') || user_can($user, 'tutor_admin')) {
                return $next($request, $response);
            }

            $mapper = new ClassesMapper();
            
            if ($mapper->belongs_to_class($user->ID, $class_id)) {
                return $next($request, $response);
            }
        }

        return $response->withStatus(403);
    }
}