<?php
namespace Vreddo\Api\Rest\Middleware;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Middleware for ensuring a user is attached to the request.
 */
class HasUser
{
    /**
     * Checks the current request for a user.
     *
     * @param Request $request
     * @param Response $response
     * @param callback $next
     * 
     * @return void
     */
    public function __invoke(Request $request, Response $response, $next) {
        // Check if there's a user in the request.
        if (!$request->getAttribute('user')) {
            return $response->withStatus(401);
        }

        return $next($request, $response);
    }
}