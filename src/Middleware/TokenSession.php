<?php
namespace Vreddo\Api\Rest\Middleware;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Middleware for parsing the Bearer token in the header.
 */
class TokenSession
{
    /**
     * Checks the current request for a token header, and includes the associated
     * user into the request.
     *
     * @param Request $request
     * @param Response $response
     * @param callback $next
     * 
     * @return void
     */
    public function __invoke(Request $request, Response $response, $next) {
        $token = null;

        // Check for an Authorization header in the request.
        $header = $request->getHeader('Authorization');
        
        if ($header) {
            $header = $header[0];

            // We only understand bearer auth.
            if (strpos($header, 'Bearer ') === 0) {
                // Grab the token from the header.
                $token = substr($header, strlen('Bearer '));
            }
        }

        // Check for a token in the query string.
        $query = $request->getQueryParams();

        if (isset($query['token'])) {
            $token = $query['token'];
        }
        
        $newRequest = $request;

        if ($token) {
            // Retrieve the associated user (if the token exists)
            $tokenMapper = new \Vreddo\Api\Rest\Models\TokenMapper();
            $token = $tokenMapper->find_token($token);

            if ($token) {
                $newRequest = $newRequest
                    ->withAttribute('token', $token)
                    ->withAttribute('user', get_user_by('id', $token->user_id));
            }
        }

        return $next($newRequest, $response);
    }
}