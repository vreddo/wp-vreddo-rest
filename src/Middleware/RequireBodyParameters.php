<?php
namespace Vreddo\Api\Rest\Middleware;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Middleware for ensuring certain body paramters are present in the body.
 */
class RequireBodyParameters
{
    protected $required;

    public function __construct($required_params) {
        $this->required = $required_params;
    }

    /**
     * Checks the current request for the body parameters.
     *
     * @param Request $request
     * @param Response $response
     * @param callback $next
     * 
     * @return void
     */
    public function __invoke(Request $request, Response $response, $next) {
        $body = $request->getParsedBody();

        if (!$body) {
            return $response->withStatus(400);
        }

        foreach ($this->required as $param) {
            if (!isset($body[$param]) || !$body[$param]) {
                return $response->withStatus(400);
            }
        }

        return $next($request, $response);
    }
}