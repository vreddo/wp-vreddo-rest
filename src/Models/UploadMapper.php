<?php
namespace Vreddo\Api\Rest\Models;

class UploadMapper extends ModelMapper
{
    /**
     * Creates a VR user content record.
     *
     * @param string $type Either "image", "video", or "audio".
     * @param string $description
     * @return int The newly created ID of the record.
     */
    public function create($class, $user, $name, $type, $description = null) {
        $new_content = wp_insert_post(array(
            'post_type' => 'vr_user_content',
            'post_title' => $name,
            'post_author' => $user
        ));

        if (is_wp_error($new_content)) {
            throw $new_content;
        }

        update_field('class_id', $class, $new_content);
        update_field('vr_content_type', $type, $new_content);
        update_field('description', $description, $new_content);

        return $new_content;
    }

    public function set_file($upload, $file) {
        update_field('file', $file, $upload);
        wp_update_post(array(
            'ID' => $upload,
            'post_status' => 'publish'
        ));
    }
}