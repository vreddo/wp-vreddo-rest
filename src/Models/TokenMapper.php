<?php
namespace Vreddo\Api\Rest\Models;

class TokenMapper extends ModelMapper
{
    public function __construct() {
        parent::__construct();
        $this->register_table('api_tokens');
    }

    /**
     * Generates the tokens table.
     *
     * @return void
     */
    public function bootstrap() {
        $db = $this->db;

        $db->query("
            CREATE TABLE IF NOT EXISTS $db->api_tokens (
                id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                token VARCHAR(32) NOT NULL, 
                user_id INT UNSIGNED NOT NULL,
                expiry_time INT UNSIGNED NOT NULL,
                status VARCHAR(10) NOT NULL,
                PRIMARY KEY (id), INDEX (user_id)
            )
        ");
    }

    /**
     * Finds a single token by its value.
     *
     * @param string $token
     * @return array
     */
    public function find_token($token) {
        $db = $this->db;

        $result = $db->get_results(
            $db->prepare("
                SELECT
                    token,
                    user_id,
                    expiry_time
                FROM
                    $db->api_tokens
                WHERE
                    token = %s AND
                    status = 'active'
            ", $token)
        );

        if (count($result) > 0) {
            $result = $result[0];
            $result->expiry = date('c', $result->expiry_time);
            unset($result->expiry_time);
            return $result;
        }

        return null;
    }

    /**
     * Finds a user ID associated with a PIN. This will return null if the PIN
     * is expired.
     *
     * @param string $pin
     * @return string
     */
    public function find_user_for_pin($pin) {
        $db = $this->db;

        $result = $db->get_col(
            $db->prepare("
            SELECT
                pinmeta.user_id
            FROM
                $db->usermeta pinmeta
            INNER JOIN
                $db->usermeta pinexpirymeta ON
                    pinexpirymeta.user_id = pinmeta.user_id AND
                    pinexpirymeta.meta_key = '_vreddo-pin-expiry'
            WHERE
                pinmeta.meta_key = '_vreddo-pin' AND
                CONVERT(pinexpirymeta.meta_value, UNSIGNED) > %d AND
                pinmeta.meta_value = %s
            ", time(), $pin)
        );

        if (count($result) > 0) {
            return $result[0];
        }
        
        return null;
    }

    /**
     * Creates a new token.
     *
     * @return object The newly created token.
     */
    public function create($token, $user, $expiry = null) {
        // By default, expiry is 24 hours.
        if (!$expiry) {
            $expiry = time() + (24 * 60 * 60);
        }

        // Insert the token into the database.
        $db = $this->db;

        $db->query(
            $db->prepare(
                "
                INSERT INTO
                    $db->api_tokens
                    (token, user_id, expiry_time, status)
                VALUES
                    (%s, %d, %d, 'active')
                ",
                $token, $user, $expiry
            )
        );

        if ($db->last_error) {
            throw $db->last_error;
        }

        // Return the token by re-retrieving it.
        return $this->find_token($token);
    }

    /**
     * Updates the token's status.
     *
     * @param string $token
     * @param string $status
     * @return void
     */
    public function update_status($token, $status) {
        $token_object = $this->find_token($token);

        if ($token_object) {
            // Update the status field.
            $db = $this->db;

            $db->query(
                $db->prepare(
                    "
                    UPDATE
                        $db->api_tokens
                    SET
                        status = %s
                    WHERE
                        token = %s
                    ",
                    $status, $token
                )
            );

            if ($db->last_error) {
                throw $db->last_error;
            }

            $token_object->status = $status;
        }

        return $token_object;
    }
}