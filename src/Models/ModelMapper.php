<?php
namespace Vreddo\Api\Rest\Models;

class ModelMapper
{
    /**
     * Interface with the database. By default, this is a $wpdb instance.
     */
    protected $db;

    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
    }

    /**
     * Adds the table to the database interface. For example, if you call
     * register_table('test'), then the value of $this->db->test will be
     * "{dbPrefix}_vreddo_test".
     *
     * @param string $tableName
     * @return void
     */
    public function register_table($tableName) {
        $this->db->$tableName = $this->db->prefix . 'vreddo_' . $tableName;
    }
}