<?php
namespace Vreddo\Api\Rest\Models;

class ClassesMapper extends ModelMapper
{
    /**
     * Retrieve all classes a user is added to.
     *
     * @param int $user
     * @return array
     */
    public function retrieve_for_user($user, $type = 'student', $with_students = false) {
        $meta_query = array(
            'relation' => 'AND',
            array(
                'key' => 'class_students',
                'value' => sprintf(':"%s";', $user),
                'compare' => 'LIKE'
            ),
        );

        if  ($type == 'trainer') {
            $meta_query = array(
				'relation' => 'AND',
				array(
					'key' => 'class_trainer',
					'value' => $user,
					'compare' => '='
				),
            );
        }

		$args = array(
			'post_type' => 'course-class',
			'meta_query'=> $meta_query
        );

        $query = new \WP_Query($args);
        $classes = array();
		
		foreach ($query->posts as $key => $class) {
			$trainer = get_field('class_trainer', $class->ID);
			
			$classes[$key]['ID'] = $class->ID;
			$classes[$key]['title'] = $class->post_title;
			$classes[$key]['public_title'] = get_field('class_public_title', $class->ID);
			$classes[$key]['room_id'] = get_field('class_room_id', $class->ID) ? intval(get_field('class_room_id', $class->ID)) : null;
			$classes[$key]['trainer']['id'] = $trainer['ID'];
			$classes[$key]['trainer']['name'] = $trainer['display_name'];
            $classes[$key]['date_time'] = (
                new \DateTime(
                    get_field('vr_class_datetime_start', $class->ID),
                    new \DateTimeZone(get_field('class_timezone', $class->ID))
                )
            )->format('c');
            
            if ($with_students) {
                foreach (get_field('class_students', $class->ID) as $student) {
                    $user_id = $student['ID'];
                    $user = get_user_by( 'id', $user_id );
                    $user_info = get_userdata($user_id);
                    
                    $class_user['id'] = $user_id;
                    $class_user['name'] = $user->data->display_name;
                    $class_user['user_login'] = $user->data->user_login;
                    $class_user['user_email'] = $user->data->user_email;
                    $class_user['role'] = implode(', ', $user_info->roles);
                    $class_user['profile_picture'] = bp_core_fetch_avatar(array('item_id' => $user_id, 'html' => false, 'type' => 'full'));
                    $class_user['headset_id'] = xprofile_get_field_data('VR Headset ID',$user_id);
        
                    $classes[$key]['course_class_students'][] = $class_user;
                }
            }
        }

        return $classes;
    }

    /**
     * Checks whether the user is the trainer of the class or is a student of
     * the class.
     *
     * @param int $user_id
     * @param int $class_id
     * @return void
     */
    public function belongs_to_class($user_id, $class_id) {
        $trainer = get_field('class_trainer', $class_id);
        $students = get_field('class_students', $class_id);

        if ($trainer['ID'] == $user_id) {
            return true;
        }

        if ($students) {
            foreach ($students as $student) {
                if ($student['ID'] == $user_id) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Retrieves media items for a particular class.
     *
     * @param int $class_id
     * @return void
     */
    public function retrieve_media($class_id) {
        $retrieved_content = get_field('vr_content', $class_id);
        return $retrieved_content;
    }
}