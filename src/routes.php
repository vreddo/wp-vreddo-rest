<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Vreddo\Api\Rest\Controllers\TokenController;
use \Vreddo\Api\Rest\Controllers\ProfileController;
use \Vreddo\Api\Rest\Controllers\ClassesController;
use \Vreddo\Api\Rest\Controllers\UploadController;
use \Vreddo\Api\Rest\Middleware\RequireBodyParameters;
use \Vreddo\Api\Rest\Middleware\HasUser;
use \Vreddo\Api\Rest\Middleware\CanAccessClass;

// Define routes here.
$app->get('/', function(Request $request, Response $response, array $args) {
    return $response->withJson(array(
        'app' => 'Vreddo API',
        'version' => '1.0'
    ));
});

// v1 routes.
$app->group('/v1', function() {
    $this->post('/token', TokenController::class . ':create')->add(new RequireBodyParameters(['PIN']));
    $this->post('/pinValid', TokenController::class . ':create')->add(new RequireBodyParameters(['PIN']));
    $this->post('/removeToken', TokenController::class . ':remove')->add(new RequireBodyParameters(['token']));
    $this->get('/profile', ProfileController::class . ':get')->add(new HasUser());
    $this->get('/classes', ClassesController::class . ':find')->add(new HasUser());
    $this->get('/classes/{class}/media', ClassesController::class . ':get_media')->add(new HasUser())->add(new CanAccessClass());
    $this->post('/uploads', UploadController::class . ':create')->add(new HasUser())
        ->add(new CanAccessClass(function($request, $response) { return $request->getParsedBody()['class']; }))
        ->add(new RequireBodyParameters(['class', 'name', 'type']));
    $this->put('/uploads/{upload}', UploadController::class . ':upload')->add(new HasUser());
    $this->post('/uploads/{upload}/complete', UploadController::class . ':complete')->add(new HasUser());
});