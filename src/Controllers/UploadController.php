<?php
namespace Vreddo\Api\Rest\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Vreddo\Api\Rest\Models\UploadMapper;

class UploadController {
    public function create(Request $request, Response $response) {
        // Extract the variables passed in the body.
        $body = $request->getParsedBody();
        extract($body);

        if (!isset($description)) {
            $description = '';
        }

        $mapper = new UploadMapper();
        $upload_id = $mapper->create($class, $request->getAttribute('user')->ID, $name, $type, $description);

        return $response->withJson(array(
            'upload_key' => $upload_id
        ));
    }

    public function upload(Request $request, Response $response, $args) {
        $body = $request->getBody();
        $size = $body->getSize();
        $i = 0;

        // Create the filename.
        if (!file_exists('../vr-user-uploads')) {
            mkdir('../vr-user-uploads');
        }

        $filename = '../vr-user-uploads/' . $args['upload'];

        // Delete any existing stored data.
        if (file_exists($filename)) {
            unlink($filename);
        }

        // Open the file for writing.
        $file = fopen($filename, 'w');

        while ($i < $size) {
            // Read 1024 bytes.
            $read = $body->read(min(1024, $size - $i));

            // Write to file.
            fwrite($file, $read);

            $i += 1024;
        }

        fclose($file);
        return 'OK';
    }

    public function complete(Request $request, Response $response, $args) {
        $upload = $args['upload'];
        $filename = '../vr-user-uploads/' . $upload;
        $mime = mime_content_type($filename);

        if (strpos($mime, 'audio/') !== 0) {
            // Create a thumbnail.
            $thumb_filename = $filename . '_thumb.jpg';
            exec("ffmpeg -y -i $filename -vf scale=" . '"' . "'if(gt(a,1/1),150,-1)':'if(gt(a,1/1),-1,150)'" . '" ' . $thumb_filename);
        }

        // Create attachment.
        $attachment = wp_insert_attachment(array(
            'post_title' => 'VR User Upload #' . $upload,
            'post_content' => '',
            'post_status' => 'inherit',
            'post_mime_type' => $mime
        ), $filename);

        $mapper = new UploadMapper();
        $mapper->set_file($args['upload'], $attachment);

        return 'OK';
    }
}