<?php
namespace Vreddo\Api\Rest\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Vreddo\Api\Rest\Models\ClassesMapper;

class ClassesController
{
    /**
     * Retrieves an array of classes for the user. By default, the listing is
     * classes the user is a student in. However, if the user is a trainer, the
     * list is of classes they're a trainer in, including students.
     *
     * @return Response
     */
    public function find(Request $request, Response $response) {
        // Get the requesting user.
        $user = $request->getAttribute('user');

        $classes_mapper = new ClassesMapper();

        // Check if they're a trainer.
        if (user_can($user, 'trainer')) {
            // Return the trainer listing.
            return $response->withJson($classes_mapper->retrieve_for_user($user->ID, 'trainer'));
        } else {
            // Return the user listing.
            return $response->withJson($classes_mapper->retrieve_for_user($user->ID));
        }
    }

    protected function get_media_array_from_file($file, $type) {
        return array(
            'ID' => $file['ID'],
            'type' => $type,
            'url' => $file['url']
        );
    }

    protected function parse_media_from_item($item) {
        switch ($item['type']) {
            case 'Image':
                if ($item['projection'] == 'flat') {
                    return $this->get_media_array_from_file($item['vr_content_file_image'], 'image');
                }

                if ($item['projection'] == '360') {
                    return $this->get_media_array_from_file($item['vr_content_file_image_360'], 'image');
                }
                break;
            case 'Video':
                if ($item['projection'] == 'flat') {
                    return $this->get_media_array_from_file($item['vr_content_file_video'], 'video');
                }

                if ($item['projection'] == '360') {
                    return $this->get_media_array_from_file($item['vr_content_file_video_360'], 'video');
                }
                break;
            case 'Hologram':
                return $this->get_media_array_from_file($item['vr_content_file_model'], 'model');
            case 'Slideshow':
                $slides = array();

                foreach ($item['slides'] as $slide) {
                    $slides[] = $this->get_media_array_from_file($slide['file'], 'image');
                }

                return $slides;
        }

        return null;
    }

    /**
     * Retrieves a list of VR content for the class.
     *
     * @return Response
     */
    public function get_media(Request $request, Response $response, $args) {
        $classes_mapper = new ClassesMapper();
        $media = array();
        
        foreach ($classes_mapper->retrieve_media($args['class']) as $item) {
            $returned_item = array(
                'ID' => intval($args['class'] . '0' . $item['vr_content_id']),
                'name' => $item['vr_content_description'],
                'type' => strtolower($item['type'])
            );

            // Include extra metadata.
            switch ($item['type']) {
                case 'Image':
                case 'Video':
                    $returned_item['projection'] = $item['projection'];
                    break;
                case 'Hologram':
                    $returned_item['material'] = $this->get_media_array_from_file($item['vr_content_file_material'], 'material');
                    break;
                case 'Slideshow':
                    $returned_item['thumbnail'] = $this->get_media_array_from_file($item['vr_content_file_thumbnail'], 'image');
                    break;
            }

            // Parse media.
            $returned_item['media'] = $this->parse_media_from_item($item);

            $media[] = $returned_item;
        }

        return $response->withJson($media);
    }
}