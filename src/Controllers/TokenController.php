<?php
namespace Vreddo\Api\Rest\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class TokenController
{
    public function create(Request $request, Response $response) {
        // Get the PIN from the request.
        $body = $request->getParsedBody();
        $pin = $body['PIN'];

        // Check the credentials.
        $token_mapper = new \Vreddo\Api\Rest\Models\TokenMapper();
        $user = $token_mapper->find_user_for_pin($pin);

        // Create the token if a user was found with that PIN.
        if ($user) {
            // Generate a random 32-character string.
            $new_token = $this->generate_token_string();
            
            // Create a new token.
            $token = $token_mapper->create($new_token, $user);
            
            // Include the user profile.
            $profile_controller = new ProfileController();
            $token->profile = $profile_controller->get_profile($user);

            // Return the newly created token.
            return $response->withJson($token);
        } else {
            return $response->withStatus(401);
        }
    }

    public function remove(Request $request, Response $response) {
        // Get the token from the request.
        $body = $request->getParsedBody();
        $token = $body['token'];

        // Attempt to update the status of the token.
        $token_mapper = new \Vreddo\Api\Rest\Models\TokenMapper();
        $token = $token_mapper->update_status($token, 'deleted');

        if ($token) {
            return $response->withJson($token);
        } else {
            return $response->withStatus(404);
        }
    }

    /**
     * Creates a randomly generated string.
     * 
     * @param int $length number of characters in the generated string
     * @return string a new string is created with random characters of the desired length
     */
    function generate_token_string($length = 32) {
        $randstr = '';

        srand((double) microtime(TRUE) * 1000000);
        //our array add all letters and numbers if you wish
        $chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 
            'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        for ($rand = 0; $rand < $length; $rand++) {
            $random = rand(0, count($chars) - 1);
            $randstr .= $chars[$random];
        }
        return $randstr;
    }
}