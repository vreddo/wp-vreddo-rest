<?php
namespace Vreddo\Api\Rest\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ProfileController
{
    public function get(Request $request, Response $response) {
        // Get the user from the request.
        $user = $request->getAttribute('user');

        // Return some fields.
        return $response->withJson($this->get_profile($user));
    }

    public function get_profile($user) {
        if (!is_object($user)) {
            $user = get_user_by('ID', $user);
        }

        // Retrieve the avatar.
        $avatar = bp_core_fetch_avatar(array(
            'item_id' => $user->ID,
            'type' => 'full',
            'html' => false
        ));

        // Determine the main role of the user.
        $role = 'user';

        if (in_array('trainer', $user->roles)) {
            $role = 'trainer';
        }

        if (in_array('administrator', $user->roles)) {
            $role = 'administrator';
        }

        return array(
            'ID' => $user->ID,
            'login' => $user->user_login,
            'display_name' => $user->display_name,
            'profile_photo' => $avatar ? $avatar : null,
            'role' => $role
        );
    }
}